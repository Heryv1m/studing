package com.micro.parse;

import java.io.*;

public class Parse {

    private final String IN = "/home/heryv1m/IdeaProjects/blanks/parser/src/resources/input/rule.txt";
    private final String OUT = "/home/heryv1m/IdeaProjects/blanks/parser/src/resources/output/rule.txt";
    private String[] pattern = new String[5];

    public static void main(String[] args) {
        Parse parse = new Parse();
        parse.createRuleFile();
    }

    public void createRuleFile() {
        File file = new File(OUT);
        try (BufferedReader br = new BufferedReader(new FileReader(IN));
             PrintWriter pw = new PrintWriter(file)) {
            while (br.ready()) {
                String validatedRow = stringValidation(br.readLine());
                pw.write(validatedRow +"\n");
            }
        } catch (FileNotFoundException except) {
            except.printStackTrace();
        } catch (IOException except) {

        }
    }

    private String stringValidation(String string) {
        String str = "";
        if (string.startsWith(" ")) {
            string = string.replaceAll("\\s+", "");
            pattern[3] = string;
            for (int i = 0; i < pattern.length; i++) {
                str = str + pattern[i];
            }
            return str;
        } else {
            string = string.replaceAll("=", "").toLowerCase();
            reloadPattern(string);
            String bage = "<!- " + string + "-->";
            return bage;
        }
    }

    private String[] reloadPattern(String string) {
        pattern[0] = "<rule ref=\"category/java/";
        pattern[1] = string;
        pattern[2] = ".xml/";
        pattern[4] = "\" />";
        return pattern;
    }
}