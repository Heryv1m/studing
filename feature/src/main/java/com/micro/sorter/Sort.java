package com.micro.sorter;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;

public class Sort {

    private final String IN = "/home/heryv1m/IdeaProjects/blanks/parser/src/resources/input/sortedrule.txt";
    private final String OUT = "/home/heryv1m/IdeaProjects/blanks/parser/src/resources/output/sorted.txt";

    public static void main(String[] args) {
        Sort sort = new Sort();
        sort.sortFile();
    }

    public void sortFile() {
        File file = new File(OUT);
        ArrayList<String> list = new ArrayList();
        try (BufferedReader br = new BufferedReader(new FileReader(IN));
             PrintWriter pw = new PrintWriter(file)) {
            while (br.ready()) {
                list.add(br.readLine());
            }
            Collections.sort(list);
            for (String str : list) {
                pw.write(str +"\n");
            }
        } catch (FileNotFoundException except) {
            except.printStackTrace();
        } catch (IOException except) {

        }
    }
}
