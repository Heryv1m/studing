package com.micro.guice;

import javax.servlet.annotation.WebFilter;

import com.google.inject.servlet.GuiceFilter;

@WebFilter("/*")
public class ScopeProviderFilter extends GuiceFilter {
}
