package com.micro.guice;

import java.util.List;

public interface ListService {
    List<String> names();
}
