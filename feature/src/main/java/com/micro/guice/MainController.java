package com.micro.guice;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.inject.Inject;
import com.google.inject.Singleton;

@Singleton
public class MainController extends HttpServlet {

    @Inject
    ListService listService;

    boolean debug = false;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (debug) {
            String str = req.getRequestURI();
            req.setAttribute("path", "hello");
            req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
        } else {
            pathResolver(req, resp);
        }
    }

    void pathResolver(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("path", req.getRequestURI() + "/");
        req.getRequestDispatcher("/WEB-INF/index.jsp").forward(req, resp);
    }
}
