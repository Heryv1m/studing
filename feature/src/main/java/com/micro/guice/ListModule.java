package com.micro.guice;

import com.google.inject.AbstractModule;

public class ListModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(ListService.class).to(ListServiceImpl.class);
    }
}
