package com.micro.guice;

import java.util.Arrays;
import java.util.List;

public class ListServiceImpl implements ListService {
    @Override
    public List<String> names() {
        return Arrays.asList("Dave", "Jimmy", "Nick");
    }
}
