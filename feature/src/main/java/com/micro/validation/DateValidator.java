package com.micro.validation;

import java.time.LocalDate;

public class DateValidator {

    /**
     * Mode 0 - check that fromDate before toDate or they equals
     * Mode 1 - check that dates in future and not equals
     * Mode 2 - check that toDate in future
     * @param fromDate start event date
     * @param toDate end event date
     * @param mode of validation
     * @return true if validation is successful
     * @throws Exception if it present
     */
    public static boolean validateFromToDates(final LocalDate fromDate, final LocalDate toDate, int mode)
            throws Exception {

        if (mode < 0 || mode > 2) {
            throw new Exception("Wrong mode");
        } else if (fromDate.isAfter(toDate)) {
            throw new Exception("fromDate is after toDate");
        } else if (mode != 0){
            modeValidation(fromDate, toDate, mode);
        }
        return true;
    }

    private static boolean modeValidation(final LocalDate fromDate, final LocalDate toDate, int mode) throws Exception {
        final LocalDate nowDate = LocalDate.now();

        if (mode == 1 && (!fromDate.isAfter(nowDate) || fromDate.isEqual(toDate))) {
            throw new Exception("Dates must be in future and not equals");
        } else if (mode == 2 && !toDate.isAfter(nowDate)) {
            throw new Exception("toDate must be in future");
        }
        return true;
    }
}