package com.micro.mq.active;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.jmx.JmxException;

import javax.jms.Connection;
import javax.jms.ExceptionListener;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

public class MessageClient implements Runnable, ExceptionListener {
  
  private String QUEUE_NAME = "TEST";
  
  @Override
  public void run() {
    try {
      ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
  
      Connection connection = factory.createConnection();
      connection.start();
      connection.setExceptionListener(this);
      
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
  
      Queue queue = session.createQueue(QUEUE_NAME);
  
      MessageConsumer consumer = session.createConsumer(queue);
  
      Message message = consumer.receive(1000);
  
      TextMessage textMessage = (TextMessage) message;
      
      String text = textMessage.getText();
  
      System.out.println("Received: " + text);
      
      consumer.close();
      session.close();
      connection.close();
    } catch (JMSException e) {
      e.printStackTrace();
    }
  }
  @Override
  public synchronized void onException(JMSException exception) {
    System.out.println("WTF!!");
  }
}
