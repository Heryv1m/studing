package com.micro.mq.active;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ActiveMain {
  
  public static void main(String[] args) {
    ScheduledExecutorService scheduled = new ScheduledThreadPoolExecutor(2);
    
    scheduled.scheduleAtFixedRate(new MessageBroker(), 0, 2, TimeUnit.SECONDS);
    scheduled.scheduleAtFixedRate(new MessageClient(), 1, 3, TimeUnit.SECONDS);
  }
}
