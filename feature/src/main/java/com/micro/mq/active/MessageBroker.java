package com.micro.mq.active;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

public class MessageBroker implements Runnable {
  
  private String QUEUE_NAME = "TEST";
  
  @Override
  public void run() {
    try {
      ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory();
  
      Connection connection = factory.createConnection();
      connection.start();
  
      Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
  
      Queue queue = session.createQueue(QUEUE_NAME);
  
      MessageProducer producer = session.createProducer(queue);
      producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
      
      String text = "Hello world!";
      TextMessage textMessage = session.createTextMessage(text);
  
      producer.send(textMessage);
      
      session.close();
      connection.close();
    } catch (JMSException e) {
      e.printStackTrace();
    }
  }
}
