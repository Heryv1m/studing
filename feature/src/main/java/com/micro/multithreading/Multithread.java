package com.micro.multithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;

public class Multithread {
  
  public static void main(String[] args) {
  
   
    ExecutorService executorService = Executors.newScheduledThreadPool(3);
    
    List<Runnable> threads = new ArrayList<>();
    threads.add(() -> {
      for (int i = 0; i < 10; i++) {
        System.out.println("hello " + i);
      }
    });
  
    threads.add(() -> {
      for (int i = 0; i < 10; i++) {
        System.out.println("go to " + (i + 1));
      }
    });
    
    threads.add(() -> {
      for (int i = 0; i < 10; i++) {
        System.out.println("buy " + i);
      }
    });
    
    System.out.println(executorService.isShutdown());
    executorService.shutdown();
    
    System.out.println(executorService.isShutdown());
  }
}
