package com.micro.validation;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DateValidatorTest {

    // Common test
    @Test
    void fromDateCantBeAfterToDateTestThrowsException_common() {
        assertThrows(Exception.class, () -> DateValidator.validateFromToDates(
                LocalDate.now().plusDays(3),
                LocalDate.now(), 0)
        );
    }

    @Test
    void wrongModeThrowsException_common() {
        assertThrows(Exception.class, () -> DateValidator.validateFromToDates(
                LocalDate.now(),
                LocalDate.now(), 3)
        );
    }

    @Test
    void fromDateIsEqualsToDateTrue_common() throws Exception {
        assertTrue(DateValidator.validateFromToDates(
                LocalDate.now().minusDays(1),
                LocalDate.now().minusDays(1), 0)
        );
    }

    @Test
    void fromDateIsBeforeToDateTrue_common() throws Exception {
        assertTrue(DateValidator.validateFromToDates(
                LocalDate.now().minusMonths(1),
                LocalDate.now().minusWeeks(1), 0)
        );
    }

    // Mode 1 test
    @Test
    void fromDateInPastThrowsException_mode1() {
        assertThrows(Exception.class, () -> DateValidator.validateFromToDates(
                LocalDate.now().minusDays(2),
                LocalDate.now().plusDays(1), 1)
        );
    }

    @Test
    void fromDateInPresentThrowsException_mode1() {
        assertThrows(Exception.class, () -> DateValidator.validateFromToDates(
                LocalDate.now(),
                LocalDate.now(), 1)
        );
    }

    @Test
    void allDatesInFutureAndSameThrowException_mode1() {
        assertThrows(Exception.class, () -> DateValidator.validateFromToDates(
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(1), 1)
        );
    }

    @Test
    void allDatesInFutureAndDiffTrue_mode1() throws Exception {
        assertTrue(DateValidator.validateFromToDates(
                LocalDate.now().plusDays(1),
                LocalDate.now().plusDays(2), 1)
        );
    }

    // Mode 2 tests
    @Test
    void toDateInPresentThrowException_mode2() {
        assertThrows(Exception.class, () -> DateValidator.validateFromToDates(
                LocalDate.now().minusDays(1),
                LocalDate.now(), 2)
        );
    }

    @Test
    void fromDatesInPastAndToDateInFutureTrue_mode2() throws Exception {
        assertTrue(DateValidator.validateFromToDates(
                LocalDate.now().minusDays(1),
                LocalDate.now().plusDays(1), 2)
        );
    }

}
