package com.micro.cdi;

import javax.naming.NamingException;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.jupiter.api.Test;

class CdiBeanTest {

    @Test
    void hello() throws NamingException {
        Injector injector = Guice.createInjector(new BindConfig());
        TextEditor editor = injector.getInstance(TextEditor.class);

        editor.makeSpellCheck();
    }
}
