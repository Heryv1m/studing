package com.vaadin.second;

import com.vaadin.Customer;
import com.vaadin.CustomerRepository;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Grid;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import org.springframework.util.StringUtils;

@SpringUI(path = "second")
public class VaadinUISecond extends UI {

    private final CustomerRepository repo;
    final Grid<Customer> grid;

    public VaadinUISecond(CustomerRepository repo) {
        this.repo = repo;
        this.grid = new Grid<>(Customer.class);
    }

    @Override
    protected void init(VaadinRequest request) {
        TextField filter = new TextField();
        filter.setPlaceholder("Filter by last name");
        filter.setValueChangeMode(ValueChangeMode.LAZY);
        filter.addValueChangeListener(e -> listCustomers(e.getValue()));
        VerticalLayout mainLayout = new VerticalLayout(filter, grid);
        setContent(mainLayout);
    }

    private void listCustomers(String filterText) {
        if (StringUtils.isEmpty(filterText)) {
            grid.setItems(repo.findAll());
        }
        else {
            grid.setItems(repo.findByLastNameStartsWithIgnoreCase(filterText));
        }
    }
}
