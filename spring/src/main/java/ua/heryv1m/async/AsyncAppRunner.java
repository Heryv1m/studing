package ua.heryv1m.async;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableAsync
@EnableScheduling
@SpringBootApplication(scanBasePackages = "ua.heryv1m.async")
public class AsyncAppRunner {
  
  public static void main(final String[] args) {
    new SpringApplicationBuilder(AsyncAppRunner.class)
      .web(WebApplicationType.NONE)
      .run(args);
  }
}
