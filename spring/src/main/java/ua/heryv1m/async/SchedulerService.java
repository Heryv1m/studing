package ua.heryv1m.async;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;

@Service
public class SchedulerService {
  
  @Autowired
  private AsyncService asyncService;
  
  private AtomicInteger threadCounter = new AtomicInteger(1);
  
  @Scheduled(cron = "0/5 * * * * *")
  public void scheduled() {
    System.out.println("Start of async thread " + threadCounter.toString());
    asyncService.doAsync(1000000000, threadCounter.getAndAdd(1));
    System.out.println("End of async thread " + threadCounter.toString());
  }
}
