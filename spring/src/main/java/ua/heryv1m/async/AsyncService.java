package ua.heryv1m.async;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class AsyncService {
  
  @Async
  public void doAsync(int cout, int threadCount) {
    for (int i = 0; i < cout; i++) {
      System.out.println("print " + threadCount + " counter " + i);
    }
  }
}
