package ua.heryv1m.scheduling;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication(scanBasePackages = "ua.heryv1m.scheduling")
public class ScheduleAppRunner {
  
  public static void main(final String[] args) {
    new SpringApplicationBuilder(ScheduleAppRunner.class)
      .web(WebApplicationType.NONE)
      .run(args);
  }
}
