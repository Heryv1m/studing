package ua.heryv1m.scheduling;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class SchedulerService {
  
  @Scheduled(cron = "0/5 * * * * *")
  public void scheduled() {
    System.out.println("Hello");
  }
}
