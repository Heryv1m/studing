package com.heryv1m.network;

import java.io.IOException;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;

public class MailSending {
    public static void main(String[] args) throws IOException, MessagingException {
        Properties properties = new Properties();
        properties.load(ClassLoader.getSystemResourceAsStream("mail.properties"));

        Session mailSession = Session.getDefaultInstance(properties);
        MimeMessage message = new MimeMessage(mailSession);
        message.setFrom("myemail");
        message.addRecipients(Message.RecipientType.TO, "youremail@mail.ru");
        message.setSubject("hello");
        message.setText("");
    }
}
