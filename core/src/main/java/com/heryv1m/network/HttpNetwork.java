package com.heryv1m.network;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class HttpNetwork {

    public static void main(String[] args) {
        try {
            URLConnection connection = new URL("http://google.com").openConnection();
            Scanner scanner = new Scanner(connection.getInputStream());
            //scanner.useDelimiter("\\Z");
            System.out.println(scanner.nextLine());

            Map<String, List<String>> headerFields = connection.getHeaderFields();
            headerFields.forEach((k, v) -> System.out.println(k + " : " + v));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
