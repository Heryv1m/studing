package com.heryv1m.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class ClientSocket {

    public static void main(String[] args) {

        String net = "india.colorado.edu";
        String localhost = "localhost";
        int port = 8100;
        int timeout = 2000;

        try (Socket socket = new Socket()){
            socket.connect(new InetSocketAddress(InetAddress.getLocalHost(), port), timeout);
            Scanner scanner = new Scanner(socket.getInputStream());
            while (scanner.hasNextLine()) {
                System.out.print(scanner.nextLine());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
