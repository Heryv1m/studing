package com.heryv1m.reflection;

public class Car {

    private int serial;
    private String color;
    private boolean production;

    public Car() {
        color = "white";
    }

    public Car(int serial, String color, boolean production) {
        this.serial = serial;
        this.color = color;
        this.production = production;
    }

    public int getSerial() {
        return serial;
    }

    public boolean isProduction() {
        return production;
    }

    public void setProduction(boolean production) {
        this.production = production;
    }

    private String printData() {
        return "Car{" +
                "serial=" + serial +
                ", color='" + color + '\'' +
                ", production=" + production +
                '}';
    }
}
