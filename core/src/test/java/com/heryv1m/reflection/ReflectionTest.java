package com.heryv1m.reflection;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ReflectionTest {

    Car car;

    @BeforeEach
    void setup() {
        car = new Car();
    }

    @Test
    void fieldReflectionTest() {

        int num = car.getSerial();
        String color = null;
        System.out.println(num + color);
        try {
            Field field = car.getClass().getDeclaredField("color");
            field.setAccessible(true);

            color = (String) field.get(car);
            System.out.println(num + color);

            field.set(car, "black");
            color = (String) field.get(car);
            System.out.println(num + color);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Test
    void methodReflectionTest() {
        try {
            Method method = car.getClass().getDeclaredMethod("printData");
            method.setAccessible(true);
            System.out.println(method.invoke(car));
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Test
    void constructorWithoutParameterReflectionTest() {
        Car car = null;

        try {
            Class clazz = Class.forName(Car.class.getName());
            car = (Car) clazz.getDeclaredConstructor().newInstance();
            System.out.println(car);
        } catch (
                IllegalAccessException | InstantiationException |  NoSuchMethodException | InvocationTargetException |
                ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Test
    void constructorWithParameterReflectionTest() {
        Car car = null;
        Class clazz = null;
        try {
            System.out.println(car);
            clazz = Class.forName(Car.class.getName());
            Class[] params = {int.class, String.class, boolean.class};
            car = (Car) clazz.getConstructor(params).newInstance(1, "black", true);
            System.out.println(car);
        } catch (InstantiationException | InvocationTargetException | NoSuchMethodException | ClassNotFoundException | IllegalAccessException e) {
            e.printStackTrace();
        }

        Constructor[] constrs = clazz.getConstructors();
        for (Constructor constr : constrs) {
            Class[] paramTypes = constr.getParameterTypes();
            for (Class paramType : paramTypes) {
                System.out.println(paramType.getName());
            }
        }
    }
}
