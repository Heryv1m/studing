package ua.sorting;

import java.util.Arrays;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static ua.sorting.Sorter.*;

public class SorterTest {

    private int[] array;

    @BeforeEach
    void setup() {
        array = new int[]{1, 3, 4, 9, 0, 9, 6, 11};
    }

    @Test
    void bubbleSorterTest() {
        bubbleSorter(array);
        System.out.println("Bubble sorting: " + Arrays.toString(array));
    }

    @Test
    void insertionSorterTest() {
        insertionSorter(array);
        System.out.println("Insertion sorting: " + Arrays.toString(array));
    }

    @Test
    void selectingSorterTest() {
        selectingSorter(array);
        System.out.println("Selection sorting: " + Arrays.toString(array));
    }

    @Test
    void mergingSorterTest() {
        mergeSorter(array);
        System.out.println("Merge sorting: " + Arrays.toString(array));
    }

    @Test
    void quickSorterTest() {
        quickSorter(array, 0, array.length - 1);
        System.out.println("Quick sorting: " + Arrays.toString(array));
    }
}
