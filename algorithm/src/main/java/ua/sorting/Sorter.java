package ua.sorting;

import java.util.Arrays;

public class Sorter {

    /**
     * Bubble sorter.
     * @param array that be sorted
     */
    public static void bubbleSorter(int[] array) {

        boolean flag = true;

        while (flag) {
            flag = false;
            for (int i = 1; i < array.length; i++) {
                if (array[i - 1] > array[i]) {
                    int temp = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = temp;
                    flag = true;
                }
            }
        }
    }

    /**
     * Insertion sorting
     * @param array that be sorted
     */
    public static void insertionSorter(int[] array) {
        for (int i = 1; i < array.length; i++) {
            int element = array[i];
            int index = i - 1;
            while (index >= 0 && array[index] > element) {
                array[index + 1] = array[index];
                index--;
            }
            array[index + 1] = element;
        }
    }

    /**
     * Selection sorting
     * @param array that be sorted
     */
    public static void selectingSorter(int[] array) {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int index = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    index = j;
                }
            }
            if (i != index) {
                int temp = array[i];
                array[i] = array[index];
                array[index] = temp;
            }
        }
    }

    /**
     * Merge sorting.
     * @param array that be sorted
     */
    public static void mergeSorter(int[] array) {
        if(array.length < 2) {
            return;
        }
        int m = array.length / 2;
        int[] left = Arrays.copyOfRange(array, 0, m);
        int[] right = Arrays.copyOfRange(array, m, array.length);
        mergeSorter(left);
        mergeSorter(right);
        merge(array, left, right);
    }

    public static void merge(int[] array, int[] left, int[] right) {
        int totalElements = left.length + right.length;
        int i,li,ri;
        i = li = ri = 0;
        while (i < totalElements) {
            if ((li < left.length) && (ri < right.length)) {
                if (left[li] < right[ri]) {
                    array[i] = left[li];
                    i++;
                    li++;
                }
                else {
                    array[i] = right[ri];
                    i++;
                    ri++;
                }
            }
            else {
                if (li >= left.length) {
                    while (ri < right.length) {
                        array[i] = right[ri];
                        i++;
                        ri++;
                    }
                }
                if (ri >= right.length) {
                    while (li < left.length) {
                        array[i] = left[li];
                        li++;
                        i++;
                    }
                }
            }
        }
    }

    /**
     * Quick sorting
     * @param array that be sorted
     */
    public static void quickSorter(int[] array, int low, int high) {
        if (low < high) {
            int newPivot = partition(array, low, high);

            quickSorter(array, low, newPivot - 1);
            quickSorter(array, newPivot + 1, high);
        }
    }

    public static int partition(int array[], int low, int high) {
        int pivot = array[high];
        int i = low - 1; // index of smaller element
        for (int j = low; j < high; j++) {
            if (array[j] <= pivot) {
                i++;
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
            }
        }
        int temp = array[i + 1];
        array[i + 1] = array[high];
        array[high] = temp;

        return i + 1;
    }
}
