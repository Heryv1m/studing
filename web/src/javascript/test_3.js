function F(p) {
    console.log(this);
    this.a = p;
}
F.prototype.b = 42;

var o = new F();

console.log(o.a);
console.log(o.b);