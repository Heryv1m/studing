package ua.heryv1m;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Accessors(fluent = true)
@Getter
@Setter
public class TgResponseMessage {

  @SerializedName("chat_id")
  private String chatId;
  @SerializedName("text")
  private String response;
  private String parseMode;
  private boolean disableWebPagePreview;
  private Integer replyToMessageId;
//  private Keyboard keyboard;
}
