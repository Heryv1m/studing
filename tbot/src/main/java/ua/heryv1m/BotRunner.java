package ua.heryv1m;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication(scanBasePackages = "ua.heryv1m")
public class BotRunner {
  
    public static void main(String[] args) {
        SpringApplication.run(BotRunner.class, args);
    }
}
