package ua.heryv1m;

import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class TelegramSender {

  private RestTemplate restTemplate;

  @Value("telegramBot.apiUrl")
  private String botUrl;
  @Value("telegramBot.token")
  private String token;

  @Autowired
  public TelegramSender(RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }

  public void sendMessage(String message) {
    String finalUrl = botUrl + token + "/sendMessage";
    TgResponseMessage mess = new Gson().fromJson(message, TgResponseMessage.class);
    mess.

    HttpEntity request = new HttpEntity();

    restTemplate.exchange(finalUrl, HttpMethod.POST, );
  }
}
