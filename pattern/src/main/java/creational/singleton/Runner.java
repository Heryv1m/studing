package creational.singleton;

public class Runner {

    public static void main(String[] args) {
        var singleton = ObjectSingleton.getInstance();
        System.out.println(singleton.doStaff());

        var single = ObjectSingleton.getInstance();
        System.out.println(single.doStaff());
    }
}
