package creational.singleton;

public class ObjectSingleton {

    private static ObjectSingleton instance;

    private ObjectSingleton(){
    }

    public static ObjectSingleton getInstance() {
        if (instance != null) {
                return instance;
        }
        instance = new ObjectSingleton();
        return instance;
    }

    String doStaff() {
        return "Some staff";
    }
}
