package creational.builder;

import creational.builder.model.Car;

public class CarAutomaticBuilder implements Builder {

    private Car car;

    @Override
    public void reset() {
        this.car = new Car();
    }

    @Override
    public void setSeats(int number) {
        car.setSeats(number);
    }

    @Override
    public void setEngine(String engine) {
        car.setEngine(engine);
    }

    @Override
    public void setTripComputer(boolean tripComputer) {
        car.setTripComputer(tripComputer);
    }

    @Override
    public void setGPS(boolean gps) {
        car.setGps(gps);
    }

    Car getResult() {
        return this.car;
    }
}
