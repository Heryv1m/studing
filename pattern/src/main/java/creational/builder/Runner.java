package creational.builder;

public class Runner {

    public static void main(String[] args) {
        var director = new Director();
        var builder = new CarAutomaticBuilder();
        director.makeSprotCar(builder);
        var car = builder.getResult();
        System.out.println(car);
    }
}
