package creational.builder;

public class Director {

    void makeSprotCar(Builder carBuilder) {
        carBuilder.reset();
        carBuilder.setSeats(2);
        carBuilder.setEngine("sportEngine");
        carBuilder.setTripComputer(true);
        carBuilder.setGPS(true);
    }
}
