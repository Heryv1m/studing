package creational.factory.model;

public class Truck implements Transport {

    public Truck() {
    }

    @Override
    public String shout() {
        return "I`m Truck!";
    }
}
