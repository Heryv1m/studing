package creational.factory.model;

public class Airplane implements Transport {

    public Airplane() {
    }

    @Override
    public String shout() {
        return "I`m Airplane!";
    }
}
