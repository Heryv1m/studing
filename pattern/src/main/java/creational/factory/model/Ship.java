package creational.factory.model;

public class Ship implements Transport {

    public Ship() {
    }

    @Override
    public String shout() {
        return "I`m Ship!";
    }
}
