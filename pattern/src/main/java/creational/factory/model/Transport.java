package creational.factory.model;

public interface Transport {

    String shout();
}
