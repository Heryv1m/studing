package creational.factory;

import creational.factory.model.Airplane;
import creational.factory.model.Transport;

public class AirplaneLogistic implements LogisticType {

    @Override
    public Transport selectTransport() {
        return new Airplane();
    }
}
