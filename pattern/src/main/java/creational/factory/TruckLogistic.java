package creational.factory;

import creational.factory.model.Transport;
import creational.factory.model.Truck;

public class TruckLogistic implements LogisticType {

    @Override
    public Transport selectTransport() {
        return new Truck();
    }
}
