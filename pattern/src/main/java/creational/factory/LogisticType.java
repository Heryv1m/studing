package creational.factory;

import creational.factory.model.Transport;

public interface LogisticType {

    Transport selectTransport();
}
