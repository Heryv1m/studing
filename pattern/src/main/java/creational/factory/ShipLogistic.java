package creational.factory;

import creational.factory.model.Ship;
import creational.factory.model.Transport;

public class ShipLogistic implements LogisticType {

    @Override
    public Transport selectTransport() {
        return new Ship();
    }
}
