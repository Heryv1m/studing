package creational.factory;

import creational.factory.model.Transport;

public class Runner {

    public Runner() {
    }

    LogisticType selectDeliveryType(String deliveryType) {
        if ("truck".equals(deliveryType)) {
            System.out.println("Truck was selected");
            return new TruckLogistic();
        } else if ("ship".equals(deliveryType)) {
            System.out.println("Ship was selected");
            return new ShipLogistic();
        } else {
            System.out.println("Airplane was selected");
            return new AirplaneLogistic();
        }
    }

    void print(Transport transport) {
        System.out.println(transport.shout());
    }

    public static void main(String[] args) {
        var factory = new Runner();

        Transport transOne = factory.selectDeliveryType("truck").selectTransport();
        var transTwo = factory.selectDeliveryType("ship").selectTransport();
        var transThree = factory.selectDeliveryType("airplane").selectTransport();

        factory.print(transOne);
        factory.print(transTwo);
        factory.print(transThree);
    }
}
