package structural.proxy;

public class CachedYoutubeClass implements ThirdPartyYoutubeLib {

    private ThirdPartyYoutubeLib service;
    private String listCache, videoCache;
    boolean needReset;

    public CachedYoutubeClass(ThirdPartyYoutubeLib service) {
        this.service = service;
    }

    @Override
    public String listVideos() {
        if (listCache == null || needReset) {
            listCache = service.listVideos();
        }
        return listCache;
    }

    @Override
    public String getVideoInfo(long id) {
        if (videoCache == null || needReset) {
            videoCache = service.getVideoInfo(id);
        }
        return videoCache;
    }

    @Override
    public void downloadVideo(long id) {
        if (!downloadExists(id) || needReset) {
            service.downloadVideo(id);
        }
    }

    private boolean downloadExists(long id) {
        return Math.random()*id/2 > 100;
    }
}
