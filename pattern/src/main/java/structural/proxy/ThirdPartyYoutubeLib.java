package structural.proxy;

/**
 * The interface of a remote service.
 */
public interface ThirdPartyYoutubeLib {

    String listVideos();
    String getVideoInfo(long id);
    void downloadVideo(long id);
}
