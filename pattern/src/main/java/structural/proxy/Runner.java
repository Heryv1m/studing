package structural.proxy;

public class Runner {

    public static void main(String[] args) {
        var youtubeService = new ThirdPartyYoutubeClass();
        var youtubeProxy = new CachedYoutubeClass(youtubeService);
        var manager = new YoutubeManager(youtubeProxy);
        manager.reactOnUserInput(3L);
    }
}
