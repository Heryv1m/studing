package structural.proxy;

/**
 * The GUI class, which used to work with a service object stays
 * unchanged. But only as long as it works with the service
 * object through an interface. We can safely pass here a proxy
 * object instead of a real service object since both of them
 * implement the same interface.
 */
public class YoutubeManager {

    protected  ThirdPartyYoutubeLib service;

    public YoutubeManager(ThirdPartyYoutubeLib service) {
        this.service = service;
    }

    public String renderVideoPage(long id) {
        return service.getVideoInfo(id);
    }

    public String renderListPanel() {
        return service.listVideos();
    }

    public void reactOnUserInput(long id) {
        renderVideoPage(id);
        renderListPanel();
    }
}
