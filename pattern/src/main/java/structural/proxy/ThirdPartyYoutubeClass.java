package structural.proxy;

public class ThirdPartyYoutubeClass implements ThirdPartyYoutubeLib {

    @Override
    public String listVideos() {
        // Send API request to Youtube.
        return "Video list";
    }

    @Override
    public String getVideoInfo(long id) {
        // Get a meta information about some video.
        return "Video Info";
    }

    @Override
    public void downloadVideo(long id) {
        // Download video file from Youtube
    }
}
