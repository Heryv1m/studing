package structural.facade.outlib;

public class BitrateReader {

    public static byte[] read(String filename, int sourceCodec) {
        byte[] buffer = new byte[2];
        buffer[0] = (byte) filename.hashCode();
        buffer[1] = (byte) filename.hashCode();
        return buffer;
    }

    public static VideoFile convert(byte[] buffer, CodecFactory distCodec) {
        var file = new VideoFile("crac");
        file.setFormat("mp4");
        return file;
    }
}
