package structural.facade.outlib;

public class CodecFactory {

    public int extract(VideoFile file) {
        return file.hashCode();
    }
}
