package structural.facade.outlib;

public class VideoFile {

    private String filename;
    private String format;

    public VideoFile(String filename) {
        this.filename = filename;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public static VideoFile create(AudioMixer file) {
        var videoFile = new VideoFile("blank");
        videoFile.setFormat("mp4");
        return videoFile;

    }
}
