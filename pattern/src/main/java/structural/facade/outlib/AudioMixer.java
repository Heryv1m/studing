package structural.facade.outlib;

public class AudioMixer {

    public AudioMixer fix (VideoFile file) {
        System.out.println("Some filtering!");
        return new AudioMixer();
    }
}
