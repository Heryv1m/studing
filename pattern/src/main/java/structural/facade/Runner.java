package structural.facade;

public class Runner {

    public static void main(String[] args) {
        var convertor = new VideoConverterFacade();
        var mp4 = convertor.convert("youtubevideo.ogg", "mp4");
    }
}
