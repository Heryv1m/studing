package structural.facade;

import structural.facade.outlib.*;

/**
 * To defeat the complexity, we create a Facade class, which
 * hides all of the framework's complexity behind a simple
 * interface. It is a trade-off between functionality and
 * simplicity.
 */
public class VideoConverterFacade {

    public VideoFile convert(String filename, String format) {
        var file = new VideoFile(filename);
        var sourceCodec = new CodecFactory().extract(file);
        CodecFactory distCodec;
        if ("mp4".equals(format)) {
            distCodec = new MPEGFourCompressionCodec();
        } else {
            distCodec = new OggCompressionCodec();
        }

        var buffer = BitrateReader.read(filename, sourceCodec);
        var result = BitrateReader.convert(buffer, distCodec);
        var mixerResult = new AudioMixer().fix(result);

        return VideoFile.create(mixerResult);
    }
}
