package structural.adapter;

public interface TypeAdapter {

    AnotherData adapteOneToAnother(OneData oneData);

    OneData adapteAnotherToOne(AnotherData data);
}
