package structural.adapter;

public class Runner {

    public static void main(String[] args) {
        var oneData = new OneData("One data string");
        var angotherData = new AnotherData("Another data string");

        var adapter = new TypeAdapterImpl();

        OneData first = adapter.adapteAnotherToOne(angotherData);
        AnotherData second = adapter.adapteOneToAnother(oneData);

        System.out.println("First data trans: " + first.getSomeData());
        System.out.println("Second data trans: " + second.getData());
    }
}
