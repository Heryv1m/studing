package structural.adapter;

public class OneData {

    private String someData;

    public OneData() {
    }

    public OneData(String someData) {
        this.someData = someData;
    }

    public String getSomeData() {
        return someData;
    }

    public void setSomeData(String someData) {
        this.someData = someData;
    }
}
