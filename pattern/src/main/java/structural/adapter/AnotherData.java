package structural.adapter;

public class AnotherData {

    private String data;

    public AnotherData() {
    }

    public AnotherData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
