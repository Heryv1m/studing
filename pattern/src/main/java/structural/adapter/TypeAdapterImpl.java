package structural.adapter;

public class TypeAdapterImpl implements TypeAdapter {

    @Override
    public AnotherData adapteOneToAnother(OneData oneData) {
        var data = new AnotherData();
        data.setData(oneData.getSomeData());
        return data;
    }

    @Override
    public OneData adapteAnotherToOne(AnotherData data) {
        var oneData = new OneData();
        oneData.setSomeData(data.getData());
        return oneData;
    }
}
