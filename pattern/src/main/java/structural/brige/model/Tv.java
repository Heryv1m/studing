package structural.brige.model;

public class Tv implements Device {

    private boolean powerOn;
    private int volume;
    private int channelOn;

    @Override
    public boolean isEnabled() {
        return this.powerOn;
    }

    @Override
    public void enable() {
        this.powerOn = true;
    }

    @Override
    public void disable() {
        this.powerOn = false;
    }

    @Override
    public int getVolume() {
        return this.volume;
    }

    @Override
    public void setVolume(int percent) {
        this.volume = percent;
    }

    @Override
    public int getChannel() {
        return this.channelOn;
    }

    @Override
    public void setChannel(int channel) {
        this.channelOn = channel;
    }

    @Override
    public String toString() {
        return "Tv{" +
                "powerOn=" + powerOn +
                ", volume=" + volume +
                ", channelOn=" + channelOn +
                '}';
    }
}
