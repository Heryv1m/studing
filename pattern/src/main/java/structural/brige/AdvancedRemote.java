package structural.brige;

import structural.brige.model.Device;

public class AdvancedRemote extends Remote {

    public AdvancedRemote() {
    }

    public AdvancedRemote(Device device) {
        super(device);
    }

    void mute() {
        device.setVolume(0);
    }
}
