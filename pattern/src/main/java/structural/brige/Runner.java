package structural.brige;

import structural.brige.model.Radio;
import structural.brige.model.Tv;

public class Runner {

    public static void main(String[] args) {
        var tv = new Tv();
        var remote = new Remote(tv);
        remote.togglePower();

        System.out.println(tv.toString());

        var radio = new Radio();
        var remoteRadio = new AdvancedRemote(radio);
        remoteRadio.togglePower();
        remoteRadio.channelUp();
        remoteRadio.volumeUp();

        System.out.println(radio.toString());

        remoteRadio.mute();

        System.out.println(radio.toString());
    }
}
