package structural.decorator;

/**
 * Concrete decorators add to a result they get from a component they wrap.
 */
public class EncryptionDecorator extends DataSourceDecorator {

    public EncryptionDecorator(DataSource wrapper) {
        super(wrapper);
    }

    @Override
    public void writeData(byte data) {
        // 1. Encrypt passed data.
        // 2. Pass encrypted data to wrappee's writeData method.
        wrapper.writeData(data);
    }

    @Override
    public byte readData() {
        // 1. Get data from wrappee's readData method.
        // 2. Try to decrypt it if it's encrypted.
        // 3. Return the result.
        return super.readData();
    }
}
