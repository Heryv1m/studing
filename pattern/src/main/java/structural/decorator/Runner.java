package structural.decorator;

/**
 * A simple example of decorator assembly.
 */
public class Runner {

    public static void main(String[] args) {
        var source = new FileDataSource("somefile.dat");
        source.writeData((byte) 96);
        // The target file has been written with plain data.

        var compDecor = new CompressionDecorator(source);
        compDecor.writeData((byte) 96);
        // The file has been written with compressed data.

        var encDecor = new EncryptionDecorator(source);
        // The source variable is now containing this:
        // Encryption > Compression > FileDataSource
        encDecor.writeData((byte) 96);
        // The file has been written with compressed and encrypted data.
    }
}
