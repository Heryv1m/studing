package structural.decorator;

public class CompressionDecorator extends DataSourceDecorator {

    public CompressionDecorator(DataSource wrapper) {
        super(wrapper);
    }

    @Override
    public void writeData(byte data) {
        // 1. Compress passed data.
        // 2. Pass compressed data to wrappee's writeData method.
        super.writeData(data);
    }

    @Override
    public byte readData() {
        // 1. Get data from wrappee's readData method.
        // 2. Try to decompress it if it's compressed.
        // 3. Return the result.
        return super.readData();
    }
}
