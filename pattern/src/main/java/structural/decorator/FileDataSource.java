package structural.decorator;

/**
 * The concrete component act as a base layer.
 */
public class FileDataSource implements DataSource {

    public FileDataSource(String fileName) {
    }

    @Override
    public void writeData(byte data) {
        // Write data to file.
    }

    @Override
    public byte readData() {
        // Read data from file.
        return 0;
    }
}
