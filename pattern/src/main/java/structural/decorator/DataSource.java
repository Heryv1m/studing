package structural.decorator;

/**
 * The common interface for all components.
 */
public interface DataSource {

    void writeData(byte data);
    byte readData();
}
