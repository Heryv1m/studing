package structural.decorator;

/**
 * The base decorator contains wrapping behavior.
 */
public class DataSourceDecorator implements DataSource {

    protected DataSource wrapper;

    public DataSourceDecorator(DataSource wrapper) {
        this.wrapper = wrapper;
    }

    @Override
    public void writeData(byte data) {
        wrapper.writeData(data);
    }

    @Override
    public byte readData() {
        return wrapper.readData();
    }
}
