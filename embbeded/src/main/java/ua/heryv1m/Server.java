package ua.heryv1m;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.startup.Tomcat;

import java.io.File;

public class Server {

    private static Tomcat tomcat = null;

    public static void main(String[] args) throws LifecycleException {
        startTomcat();
    }

    public static void startTomcat() throws LifecycleException {
        tomcat = new Tomcat();
        tomcat.getConnector();
        tomcat.setPort(8080);
        tomcat.setHostname("localhost");
        String appBase = ".";
        tomcat.getHost().setAppBase(appBase);

        File docBase = new File(System.getProperty("java.io.tmpdir"));
        Context ctx = tomcat.addContext("", docBase.getAbsolutePath());

        // add a servlet
        Class dispServlet = DispatcherServlet.class;
        Tomcat.addServlet(ctx, dispServlet.getSimpleName(), dispServlet.getName());
        ctx.addServletMapping("/", dispServlet.getSimpleName());

            // add a filter and filterMapping
            /*Class filterClass = MyFilter.class;
            FilterDef myFilterDef = new FilterDef();
            myFilterDef.setFilterClass(filterClass.getName());
            myFilterDef.setFilterName(filterClass.getSimpleName());
            context.addFilterDef(myFilterDef);*/

            /*FilterMap myFilterMap = new FilterMap();
            myFilterMap.setFilterName(filterClass.getSimpleName());
            myFilterMap.addURLPattern("/my-servlet/*");
            context.addFilterMap(myFilterMap);*/

        tomcat.start();
            // uncomment for live test
        tomcat.getServer().await();
    }
}
