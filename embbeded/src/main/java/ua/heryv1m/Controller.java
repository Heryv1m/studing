package ua.heryv1m;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface Controller {

    void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException;

    void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException;
}
