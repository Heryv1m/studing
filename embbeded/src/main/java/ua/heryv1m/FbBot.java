package ua.heryv1m;

import com.google.gson.Gson;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import ua.heryv1m.facebook.FbMessage;
import ua.heryv1m.facebook.FbPostBack;
import ua.heryv1m.facebook.ResponceMessage;
import ua.heryv1m.facebook.ResponceMessageText;
import ua.heryv1m.facebook.entity.Recipient;

import java.io.IOException;


public class FbBot {

    private String baseUrl = "https://graph.facebook.com/v3.1/me";
    private String pageToken = "EAAb3bp5DyZBoBAHHLaxBkFpKHBdZBdZAfwYtUSbhtkeZCtuqoVdEsleOx0IN09xZArmeXr76hWAWfAKKdE0OYptG2FKT3JG5pw5UhCwrEpCkqJoazXkM1SE4SP0adZAVPTmUToDHwC9MX1MYUz1zJ12Ox13FmapHnafuU8hAKn0kafoegABEZCM";
    private final String CONTENT_TYPE = "application/json";
    private final String CHARSET = "utf-8";
    private String recipientId;
    private String text;
    private JSONArray quickReplyButtons;
    private JSONObject buttonTemplate;
    private JSONObject genericTemplate;

    public void handlePostBack(FbPostBack postBack) {

    }

    public void handleMessage(FbMessage message) {
        //Recipient recipient = message.getEntries().get(0).getMessaging().get(0).getRecipient();
        Recipient recipient = new Recipient();
        recipient.setId(message.getEntries().get(0).getMessaging().get(0).getSender().getId());
        ResponceMessageText rmt = new ResponceMessageText();
        rmt.setText("Hi");
        ResponceMessage rm = new ResponceMessage("RESPONSE", recipient, rmt);
        String json = new Gson().toJson(rm);

        send(json);
    }

    private void send(String payload) {
        String finalUrl = baseUrl
                + (baseUrl.endsWith("/") ? "messages?access_token=" : "/messages?access_token=")
                + pageToken;

        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(finalUrl);

        StringEntity entity = null;
        try {
            entity = new StringEntity(payload);
            entity.setContentType(CONTENT_TYPE);
            post.setEntity(entity);

            CloseableHttpResponse response = client.execute(post);
            if (response != null) {
                System.out.println(response.getEntity().getContent().toString());
            }
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
