package ua.heryv1m;


import com.google.gson.Gson;
import org.json.JSONObject;
import ua.heryv1m.facebook.FbMessage;
import ua.heryv1m.facebook.FbPostBack;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FacebookController implements Controller {

    FbBot fbBot = new FbBot();

    String reqToken = "EAAb3bp5DyZBoBAHHLaxBkFpKHBdZBdZAfwYtUSbhtkeZCtuqoVdEsleOx0IN09xZArmeXr76hWAWfAKKdE0OYptG2FKT3JG5pw5UhCwrEpCkqJoazXkM1SE4SP0adZAVPTmUToDHwC9MX1MYUz1zJ12Ox13FmapHnafuU8hAKn0kafoegABEZCM";
    String respToken = "token";
    String app_id = "1960904333970410";
    String app_secret_key = "c4ec2aeb18b42927abea8119eeed3411";
    String client_marker = "158ab38e3810b011649d45ba5338e7ed";

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String jsonString = request.getReader().readLine();
        JSONObject messageJson = new JSONObject(jsonString);
        if (messageJson.has("postback")) {
            FbPostBack postBack = new Gson().fromJson(messageJson.toString(), FbPostBack.class);
            fbBot.handlePostBack(postBack);
        } else {
            FbMessage message = new Gson().fromJson(messageJson.toString(), FbMessage.class);
            fbBot.handleMessage(message);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestMode = request.getParameter("hub.mode");
        String requestToken = request.getParameter("hub.verify_token");
        String requestChallenge = request.getParameter("hub.challenge");
        if (requestMode.equals("subscribe") && requestToken.equals(reqToken)) {
            response.setContentType("application/json");
            response.getWriter().print(requestChallenge);
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            response.setContentType("application/json");
            response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        }
    }
}
