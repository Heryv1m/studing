package ua.heryv1m;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ControllerHolder {

    private static Map<String, Controller> controllers= new HashMap<>();

    static {
        controllers.put("/facebook", new FacebookController());
    }

    void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String path = getPath(req);
        if (controllers.containsKey(path)) {
            controllers.get(path).doPost(req, resp);
        } else {
            throw new IOException("Path not exist");
        }
    }

    void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String path = getPath(req);
        if (controllers.containsKey(path)) {
            controllers.get(path).doGet(req, resp);
        } else {
            throw new IOException("Path not exist");
        }
    }

    private String getPath(HttpServletRequest req) {
        return req.getServletPath();
    }
}
