package ua.heryv1m.facebook;

import java.util.List;

public class FbMessage {
    String object;
    List<Entry> entry;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public List<Entry> getEntries() {
        return entry;
    }

    public void setEntries(List<Entry> entries) {
        this.entry = entries;
    }
}
