package ua.heryv1m.facebook;

import ua.heryv1m.facebook.entity.Recipient;
import ua.heryv1m.facebook.entity.Sender;

import java.util.List;

public class Messaging {
    Sender sender;
    Recipient recipient;
    long timestamp;
    Message message;

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Message getMessages() {
        return message;
    }

    public void setMessages(Message messages) {
        this.message = messages;
    }
}
