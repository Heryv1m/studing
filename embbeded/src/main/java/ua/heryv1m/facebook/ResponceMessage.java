package ua.heryv1m.facebook;

import com.google.gson.annotations.SerializedName;
import ua.heryv1m.facebook.entity.Recipient;

public class ResponceMessage {

    @SerializedName("messaging_type")
    String messagingType;
    Recipient recipient;
    ResponceMessageText message;

    public ResponceMessage(String messagingType, Recipient recipient, ResponceMessageText message) {
        this.messagingType = messagingType;
        this.recipient = recipient;
        this.message = message;
    }
}
